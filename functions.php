<?php 

  //funcion titulo proyecto
  function init_template(){

    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');

    //registro el menu
    register_nav_menus(
       array(
         'top_menu' => 'Menu principal',
         'logo_menu' => 'logo menu'
       )
       );

  }

  add_action('after_setup_theme','init_template');




  //declaro mis estilos y scripts
  function theme_scripts() {
    
    //registro de estilos
    wp_register_style('main', get_template_directory_uri() . '/css/main.css', array());
    wp_register_style( 'bootstrap',  get_template_directory_uri() . '/css/lib/bootstrap.min.css', array(), '5.0.2'); 


    //llamar estilos
    wp_enqueue_style('main');
    wp_enqueue_style('bootstrap');


    //Registrar scripts
    wp_register_script( 'bootstrap',  get_template_directory_uri() . '/js/lib/bootstrap.min.js', array('jquery'), '5.0.2',true); 
    wp_register_script( 'main', get_template_directory_uri() . '/js/main.js', array(), '', true );

    //Llamar scripts
    wp_enqueue_script('bootstrap');
    wp_enqueue_script( 'main' );

  }

  add_action('wp_enqueue_scripts', 'theme_scripts');




  // menu de navegacion
  function universo_nav()
  {
    wp_nav_menu(
    array(
      'theme_location'  => 'top_menu',
      'menu_class' => 'menu_principal',
      'container_class' => 'container-menu',
      )
    );
  }

  //widgets

  function widget_logo(){
  }


?>