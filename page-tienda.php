<?php get_header(); ?>

<main class="main">
  
    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

  <section>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1><?php the_title(); ?></h1>
        </div>
      </div>
    </div>
  </section>


  <section class="Categoria-sol">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
         <?php the_content(); ?>
        </div>
      </div>
    </div>
  </section>

  <?php endwhile; ?>

  <?php else: ?>


  <?php endif; ?>


<?php get_footer(); ?>