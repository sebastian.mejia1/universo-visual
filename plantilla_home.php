<?php
/*
Template Name: Plantilla Home
*/
?>

<?php get_header(); ?>

<main >
  <section class="bannerHome">
      <?php echo do_shortcode ('[smartslider3 slider = "2"]');?>
  </section>


  <section class="Descripcion_home">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="Title_Section text-center">
            <h1 class="Title">
              <?php the_title(); ?>
            </h1>
          </div>  
        </div>
      </div>
    </div>
  </section>

  <section class="page-home">
    <div class="content-page-home">
      <?php the_content(); ?>

    </div>
  </section>


</main>

<?php get_footer(); ?>