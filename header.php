<!DOCTYPE html>
<html class="html" lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head();?>
</head>
<body>

 <header class="header">
    <div class="ancho-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2">
            <div class="logo">
              <a href="http://localhost/universo-visual/2021/wordpress/">
              <img src="<?php echo get_template_directory_uri() ?>/img/logo.svg" alt="">
              </a> 
              </div>
            </div>

          <div class="col-md-8">
            <nav class="content_list">
              <?php universo_nav(); ?>
            </nav></div>

          <div class="col-md-2">
            <div class="content_shop">
              <button><img src="<?php echo get_template_directory_uri() ?>/img/search.png" alt=""></button>
              <a href="http://localhost/universo-visual/2021/wordpress/carrito/" class="purchase">
                <img src="<?php echo get_template_directory_uri() ?>/img/shop.png" alt="">
                <p class="contador">0</p></div>
              </a>
            <div> 
          </div>
        </div>
      </div>  
    </div>
 </header>
