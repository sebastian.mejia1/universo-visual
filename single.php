<?php get_header(); ?>
  <main class="main">
    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

    <div class="single-page" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h1><?php the_title(); ?></h1>
            </div>
          </div>
        </div>
      </div>

      <section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="container">
          <div class="row">
            <div class="col-md-12">
            <?php the_content(); ?>
            </div>
          </div>
				</div>
			</section>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<section>

				<h2><?php _e( 'Lo siento, no hay nada que mostrar.'); ?></h2>

			</section>
			<!-- /article -->

		<?php endif; ?>
	</main>


<?php get_footer(); ?>